import tkinter as tk
from tkinter import simpledialog, messagebox, ttk
import requests

SERVER_URL = "http://localhost:5000"

class Application(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Database Management System Client")
        self.geometry("800x600")
        self.create_widgets()

    def create_widgets(self):
        ttk.Button(self, text="Create Database", command=self.create_database).pack()
        ttk.Button(self, text="Drop Database", command=self.drop_database).pack()
        ttk.Button(self, text="Show Databases", command=self.show_databases).pack()

        self.tree = ttk.Treeview(self, columns=('Database',), show='headings')
        self.tree.heading('Database', text='Database')
        self.tree.pack(fill=tk.BOTH, expand=True)

    def create_database(self):
        db_name = simpledialog.askstring("Input", "Database Name:", parent=self)
        if db_name:
            response = requests.post(f"{SERVER_URL}/create_database", json={'database_name': db_name})
            try:
                print(response)
                messagebox.showinfo("Server Response", response.json().get('message'))
            except ValueError:
                messagebox.showerror("Error", "Invalid response received from server: " + response.text)

    def drop_database(self):
        db_name = simpledialog.askstring("Input", "Database Name:", parent=self)
        if db_name:
            response = requests.post(f"{SERVER_URL}/drop_database", json={'database_name': db_name})
            try:
                messagebox.showinfo("Server Response", response.json().get('message'))
            except ValueError:
                messagebox.showerror("Error", "Invalid response received from server: " + response.text)

    def show_databases(self):
        response = requests.get(f"{SERVER_URL}/show_databases")
        if response.ok:
            self.tree.delete(*self.tree.get_children())
            for database in response.json().get('databases', []):
                self.tree.insert("", 'end', values=(database,))
        else:
            messagebox.showinfo("Server Response", "Failed to fetch databases")

if __name__ == "__main__":
    app = Application()
    app.mainloop()

from dbms_core import MiniDBMS
from query_processor import QueryProcessor

def main():
    dbms = MiniDBMS()
    query_processor = QueryProcessor()

    print(dbms.create_database("university"))
    print(dbms.create_table("university", "students", {"id": "int", "name": "str"}, "id", ["name"]))
    
    print(query_processor.insert_into_table("university", "students", {"id": 1, "name": "John Doe"}))
    print(query_processor.insert_into_table("university", "students", {"id": 2, "name": "Jane Doe"}))


if __name__ == "__main__":
    main()

import json
import os

class IndexManager:
    def __init__(self, db_directory="databases"):
        self.db_directory = db_directory

    def create_index(self, db_name, table_name, column_name, unique=False):
        index_path = os.path.join(self.db_directory, db_name, f"{table_name}_{column_name}_index.json")
        if os.path.exists(index_path):
            return "Index already exists."
        with open(index_path, 'w') as index_file:
            json.dump({}, index_file)
        return f"Index on {column_name} created for table {table_name}."

    def update_index(self, db_name, table_name, column_name, key, value):
        index_path = os.path.join(self.db_directory, db_name, f"{table_name}_{column_name}_index.json")
        if not os.path.exists(index_path):
            self.create_index(db_name, table_name, column_name)
        with open(index_path, 'r+') as index_file:
            index = json.load(index_file)
            if key in index:
                return "Value already exists in unique index."
            index[key] = value
            index_file.seek(0)
            json.dump(index, index_file)
        return "Index updated."

    def query_index(self, db_name, table_name, column_name, key):
        index_path = os.path.join(self.db_directory, db_name, f"{table_name}_{column_name}_index.json")
        if not os.path.exists(index_path):
            return None
        with open(index_path, 'r') as index_file:
            index = json.load(index_file)
            return index.get(key)

import os
import json

class MiniDBMS:
    def __init__(self, db_file="databases.json"):
        self.db_file = db_file
        if not os.path.exists(db_file):
            with open(db_file, 'w') as f:
                json.dump([], f)

    def read_databases(self):
        with open(self.db_file, 'r') as file:
            return json.load(file)

    def write_databases(self, databases):
        with open(self.db_file, 'w') as file:
            json.dump(databases, file)

    def create_database(self, name):
        databases = self.read_databases()
        if any(db['name'] == name for db in databases):
            return "Database already exists."
        databases.append({"name": name, "tables": []})
        self.write_databases(databases)
        return f"Database '{name}' created successfully."

    def drop_database(self, name):
        databases = self.read_databases()
        new_dbs = [db for db in databases if db['name'] != name]
        if len(new_dbs) == len(databases):
            return "Database does not exist."
        self.write_databases(new_dbs)
        return f"Database '{name}' dropped successfully."

    def create_table(self, db_name, table_name, schema, primary_key=None, unique_keys=None, foreign_keys=None, indexes=None):
        unique_keys = unique_keys if unique_keys is not None else []
        foreign_keys = foreign_keys if foreign_keys is not None else []
        indexes = indexes if indexes is not None else []
        databases = self.read_databases()
        db = next((db for db in databases if db['name'] == db_name), None)
        if db is None:
            return "Database does not exist."
        if any(table['name'] == table_name for table in db['tables']):
            return "Table already exists."
        table_data = {
            "name": table_name,
            "attributes": schema,
            "primary-key": primary_key,
            "unique-keys": unique_keys,
            "foreign-keys": foreign_keys,
            "indexes": indexes
        }
        db['tables'].append(table_data)
        self.write_databases(databases)
        return f"Table '{table_name}' created successfully in database '{db_name}'."

    def drop_table(self, db_name, table_name):
        databases = self.read_databases()
        db = next((db for db in databases if db['name'] == db_name), None)
        if db is None:
            return "Database does not exist."
        new_tables = [table for table in db['tables'] if table['name'] != table_name]
        if len(new_tables) == len(db['tables']):
            return "Table does not exist."
        db['tables'] = new_tables
        self.write_databases(databases)
        return f"Table '{table_name}' dropped successfully."
    def get_database(self, name):
        databases = self.read_databases()
        return next((db for db in databases if db['name'] == name), None)
    def list_databases(self):
        return self.read_databases()  

import os
import json

class QueryProcessor:
    def __init__(self, db_directory="databases"):
        self.db_directory = db_directory

    def insert_into_table(self, db_name, table_name, row_data):
        table_path = os.path.join(self.db_directory, db_name, f"{table_name}.json")
        if not os.path.exists(table_path):
            return "Table does not exist."
        with open(table_path, 'r+') as file:
            data = json.load(file)
            file.seek(0)
            file.truncate()
            # Check for primary key violation
            if data['primary_key'] and any(row[data['primary_key']] == row_data[data['primary_key']] for row in data['rows']):
                return "Primary key violation."
            # Check for unique key violations
            for key in data['unique_keys']:
                if any(row[key] == row_data[key] for row in data['rows']):
                    return f"Unique key violation on {key}."
            # Enforcing foreign key constraints
            for fk in data['foreign_keys']:
                fk_table_path = os.path.join(self.db_directory, db_name, f"{fk}.json")
                if os.path.exists(fk_table_path):
                    with open(fk_table_path, 'r') as fk_file:
                        fk_data = json.load(fk_file)
                        if not any(fk_row[data['primary_key']] == row_data[fk] for fk_row in fk_data['rows']):
                            return f"Foreign key violation on {fk}."
            data['rows'].append(row_data)
            json.dump(data, file)
            return "Row inserted successfully."

    def delete_from_table(self, db_name, table_name, condition):
        table_path = os.path.join(self.db_directory, db_name, f"{table_name}.json")
        if not os.path.exists(table_path):
            return "Table does not exist."
        with open(table_path, 'r+') as file:
            data = json.load(file)
            file.seek(0)
            file.truncate()
            initial_rows = len(data['rows'])
            data['rows'] = [row for row in data['rows'] if not eval(condition)]
            json.dump(data, file)
            return f"Deleted {initial_rows - len(data['rows'])} rows."

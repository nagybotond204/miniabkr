from flask import Flask, request, jsonify
from pymongo import MongoClient
import traceback
import json

app = Flask(__name__)
client = MongoClient('mongodb://localhost:27017')

# Load catalog from the JSON file at startup
with open('databases.json', 'r') as file:
    catalog = json.load(file)

@app.errorhandler(Exception)
def handle_exception(e):
    # Log the error and stack trace for debugging
    traceback.print_exc()
    # Ensure a JSON response with the error description
    return jsonify({'error': 'Server error', 'details': str(e)}), 500

@app.route('/create_database', methods=['POST'])
def create_database():
    try:
        db_name = request.json().get('database_name')
        if db_name is None or db_name.strip() == '':
            return jsonify({'error': 'Database name is required'}), 400
        client[db_name]  # Implicitly create the database in MongoDB
        return jsonify({'message': f'Database {db_name} created successfully'})
    except Exception as e:
        return jsonify({'error': str(e)}), 400

@app.route('/drop_database', methods=['POST'])
def drop_database():
    try:
        db_name = request.json.get('database_name')
        client.drop_database(db_name)
        return jsonify({'message': f'Database {db_name} dropped successfully'})
    except Exception as e:
        return jsonify({'error': str(e)}), 400

@app.route('/show_databases', methods=['GET'])
def show_databases():
    databases = client.list_database_names()
    return jsonify({'databases': databases})

if __name__ == '__main__':
    app.run(debug=True, port=5000)

from pymongo import MongoClient
import pymongo

class MongoDBManager:
    def __init__(self, uri="mongodb://localhost:27017/"):
        self.client = MongoClient(uri)
        self.db = None

    def connect_to_database(self, db_name):
        self.db = self.client[db_name]

    def create_collection(self, collection_name):
        return self.db[collection_name]

    def insert_document(self, collection_name, document):
        collection = self.create_collection(collection_name)
        collection.insert_one(document)

    def find_documents(self, collection_name, query):
        collection = self.create_collection(collection_name)
        return collection.find(query)

    def update_document(self, collection_name, query, new_values):
        collection = self.create_collection(collection_name)
        collection.update_one(query, {"$set": new_values})

    def delete_document(self, collection_name, query):
        collection = self.create_collection(collection_name)
        collection.delete_one(query)

    def setup_collections(self, database_name, table_info):
        self.connect_to_database(database_name)
        for table in table_info:
            collection = self.create_collection(table['name'])
            # Creating indexes based on the 'index' field in JSON
            for idx, field in enumerate(table.get('index', [])):
                collection.create_index([(field, pymongo.ASCENDING)], name=table['indexNames'][idx])
            # Ensure unique constraints on fields specified as 'unique-keys'
            for unique_key in table.get('unique-keys', []):
                collection.create_index([(unique_key, pymongo.ASCENDING)], unique=True)

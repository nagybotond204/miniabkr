from pymongo import MongoClient
import pymongo

class MongoDBManager:
    def __init__(self, uri="mongodb://localhost:27017/"):
        self.client = MongoClient(uri)
        self.db = None

    def connect_to_database(self, db_name):
        self.db = self.client[db_name]

    def create_collection(self, collection_name):
        return self.db[collection_name]

    def insert_document(self, collection_name, document):
        collection = self.create_collection(collection_name)
        collection.insert_one(document)

    def find_documents(self, collection_name, query):
        collection = self.create_collection(collection_name)
        return collection.find(query)

    def update_document(self, collection_name, query, new_values):
        collection = self.create_collection(collection_name)
        collection.update_one(query, {"$set": new_values})

    def delete_document(self, collection_name, query):
        collection = self.create_collection(collection_name)
        collection.delete_one(query)

    def setup_collections(self, database_name, table_info):
        self.connect_to_database(database_name)
        for table in table_info:
            collection = self.create_collection(table['name'])
            for idx, field in enumerate(table.get('index', [])):
                collection.create_index([(field, pymongo.ASCENDING)], name=table['indexNames'][idx])
            for unique_key in table.get('unique-keys', []):
                collection.create_index([(unique_key, pymongo.ASCENDING)], unique=True)
                
    def insert_document(self, collection_name, document):
        collection = self.db[collection_name]
        return collection.insert_one(document).inserted_id

    def delete_document(self, collection_name, criteria):
        collection = self.db[collection_name]
        return collection.delete_many(criteria).deleted_count

    def update_document(self, collection_name, criteria, update_values):
        collection = self.db[collection_name]
        # Ensure that update_values is structured correctly
        update_document = {'$set': update_values}
        result = collection.update_one(criteria, update_document)
        return result.modified_count

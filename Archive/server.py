from flask import Flask, request, jsonify
# from dbms_core import MiniDBMS
from mongodb_manager import MongoDBManager
from db_sync import DBSync
import json

app = Flask(__name__)
dbms = MongoDBManager() # Assuming dbms is properly initialized in your environment
db_sync = DBSync() 

@app.before_first_request
def initialize_database():
    """Pull initial data from MongoDB to synchronize the local JSON file."""
    db_sync.pull_data_from_mongodb()
#upload the pulled data to the json file using db_sync.py
@app.route('/upload_data', methods=['POST'])
def upload_data():
    db_sync.pull_data_from_mongodb()
    return jsonify({'message': 'Data uploaded successfully'})

@app.route('/create_database', methods=['POST'])
def create_database():
    data = request.json
    response = dbms.create_database(data['name'])
    return jsonify({'message': response})

@app.route('/drop_database', methods=['POST'])
def drop_database():
    data = request.json
    response = dbms.drop_database(data['name'])
    return jsonify({'message': response})

@app.route('/create_table', methods=['POST'])
def create_table():
    data = request.json
    response = dbms.create_table(data['database'], data['table_name'], data['schema'],
                                 data.get('primary_key'), data.get('unique_keys'), data.get('foreign_keys'), data.get('indexes'))
    return jsonify({'message': response})

@app.route('/drop_table', methods=['POST'])
def drop_table():
    data = request.json
    response = dbms.drop_table(data['database'], data['table_name'])
    return jsonify({'message': response})

@app.route('/get_tables_with_columns', methods=['GET'])
def get_tables_with_columns():
    db_name = request.args.get('database')
    if not db_name:
        return jsonify({'error': 'Database name is required'}), 400
    database = dbms.get_database(db_name)
    if not database:
        return jsonify({'error': 'Database not found'}), 404
    tables_info = []
    for table in database['tables']:
        table_info = {
            'name': table['name'],
            'columns': [{'name': col_name, 'type': col_type} for col_name, col_type in table['schema'].items()]
        }
        tables_info.append(table_info)
    return jsonify(tables_info)

@app.route('/list_databases', methods=['GET'])
def list_databases():
    databases = dbms.list_databases()  # Assuming a method that lists all databases
    return jsonify([{'name': db['name']} for db in databases])

@app.route('/get_tables', methods=['GET'])
def get_tables():
    db_name = request.args.get('database')
    if not db_name:
        return jsonify({'error': 'Database name is required'}), 400
    try:
        database = dbms.get_database(db_name)
        if not database:
            return jsonify({'error': 'Database not found'}), 404
        tables_data = [
            {
                "name": table['name'],
                "rows": [{"column_name": key, "value": value} for key, value in table.get('data', {}).items()]
            } for table in database['tables']
        ]
        return jsonify(tables_data)
    except Exception as e:
        return jsonify({'error': str(e)}), 500
@app.route('/insert', methods=['POST'])
def insert_document():
    data = request.json
    result = MongoDBManager.insert_document(data['collection'], data['document'])
    return jsonify({'inserted_id': str(result)})

@app.route('/delete', methods=['POST'])
def delete_document():
    data = request.json
    result = MongoDBManager.delete_document(data['collection'], data['criteria'])
    return jsonify({'deleted_count': result})

@app.route('/update', methods=['POST'])
def update_document():
    data = request.json
    result = MongoDBManager.update_document(data['collection'], data['criteria'], data['update_values'])
    return jsonify({'modified_count': result})

if __name__ == '__main__':
    #db_sync.pull_data_from_mongodb()
    db_sync.push_data_to_mongodb()
    app.run(debug=True)
# Path: server.py
from pymongo import MongoClient

class IndexManager:
    def __init__(self, uri="mongodb://localhost:27017/", db_name="your_database"):
        self.client = MongoClient(uri)
        self.db = self.client[db_name]

    def create_unique_index(self, collection_name, field_name):
        collection = self.db[collection_name]
        collection.create_index([(field_name, 1)], unique=True)
        return f"Unique index on {field_name} created for {collection_name}."

    def update_non_unique_index(self, collection_name, field_name, doc_id, value):
        index_collection = self.db[f"{collection_name}_{field_name}_index"]
        index_collection.update_one({"value": value}, {"$addToSet": {"doc_ids": doc_id}}, upsert=True)

    def check_foreign_key_constraints(self, ref_collection, ref_field, value):
        ref_docs = self.db[ref_collection].find({ref_field: value})
        return list(ref_docs)

    def delete_document(self, collection_name, criteria):
        if self.check_foreign_key_constraints("other_collection", "ref_field", criteria["_id"]):
            return "Cannot delete document due to foreign key constraints."
        result = self.db[collection_name].delete_one(criteria)
        return f"Document deleted: {result.deleted_count}."

    def query_index(self, collection_name, field_name, key_value):
        collection = self.db[collection_name]
        return list(collection.find({field_name: key_value}))

    def cleanup_indexes(self, collection_name):
        index_collection = self.db[f"{collection_name}_indexes"]
        index_collection.delete_many({"last_accessed": {"$lt": "certain_date"}})

    def insert_document(self, collection_name, document):
        collection = self.db[collection_name]
        result = collection.insert_one(document)
        self.update_index_on_insert(collection_name, document)
        return result.inserted_id

    def update_index_on_insert(self, collection_name, document):
        index_collection = self.db[f'{collection_name}_index']
        for key, value in document.items():
            if isinstance(value, list):
                for item in value:
                    index_collection.update_one({'key': key, 'value': item}, {'$addToSet': {'primary_keys': document['_id']}}, upsert=True)
            else:
                index_collection.update_one({'key': key, 'value': value}, {'$set': {'primary_key': document['_id']}}, upsert=True)

    def update_index_on_delete(self, collection_name, document_id):
        index_collection = self.db[f'{collection_name}_index']
        document = self.db[collection_name].find_one({'_id': document_id})
        for key, value in document.items():
            if isinstance(value, list):
                for item in value:
                    index_collection.update_one({'key': key, 'value': item}, {'$pull': {'primary_keys': document_id}})
            else:
                index_collection.delete_one({'key': key, 'value': value})
    def update_document(self, collection_name, criteria, update_values):
        """Update a document and maintain index integrity."""
        collection = self.db[collection_name]
        result = collection.update_one(criteria, update_values)
        if result.matched_count > 0 and 'email' in update_values.get('$set', {}):
            # Update index if the email was part of the update
            self.update_non_unique_index(collection_name, 'email', criteria['_id'], update_values['$set']['email'])
        return result.modified_count


    def update_indexes_after_document_update(self, collection_name, criteria, update_values):
        """ Update indexes after a document has been updated. """
        # Extract fields being updated and update respective indexes if necessary
        updated_fields = update_values.get('$set', {})
        for field, value in updated_fields.items():
            self.update_non_unique_index(collection_name, field, criteria['_id'], value)


if __name__ == "__main__":
    index_manager = IndexManager(db_name="test_db")
    print(index_manager.create_unique_index("test_table", "unique_field"))
    print(index_manager.query_index("test_table", "unique_field", "some_key_value"))
    print(index_manager.delete_document("test_table", {"_id": "doc123"}))

import tkinter as tk
from tkinter import ttk, simpledialog, messagebox
import requests

SERVER_URL = "http://localhost:5000"

class CreateTableFrame(tk.Frame):
    def __init__(self, parent):
        super().__init__(parent)
        self.attributes = []
        self.init_ui()

    def init_ui(self):
        self.db_name_label = tk.Label(self, text="Database Name:")
        self.db_name_label.grid(row=0, column=0)
        self.db_name_entry = tk.Entry(self)
        self.db_name_entry.grid(row=0, column=1)

        self.table_name_label = tk.Label(self, text="Table Name:")
        self.table_name_label.grid(row=1, column=0)
        self.table_name_entry = tk.Entry(self)
        self.table_name_entry.grid(row=1, column=1)

        self.add_column_button = tk.Button(self, text="Add Column", command=self.add_column)
        self.add_column_button.grid(row=2, column=0, columnspan=2)

        self.columns_frame = tk.Frame(self)
        self.columns_frame.grid(row=3, column=0, columnspan=2, sticky="nsew")

        self.create_table_button = tk.Button(self, text="Create Table", command=self.create_table)
        self.create_table_button.grid(row=4, column=0, columnspan=2)

    def add_column(self):
        column_frame = tk.Frame(self.columns_frame)
        column_frame.pack(fill='x')

        name_entry = tk.Entry(column_frame)
        name_entry.pack(side='left')
        data_type_entry = tk.Entry(column_frame)
        data_type_entry.pack(side='left')

        pk_var = tk.BooleanVar()
        primary_key_check = tk.Checkbutton(column_frame, text='PK', variable=pk_var)
        primary_key_check.pack(side='left')

        unique_var = tk.BooleanVar()
        unique_key_check = tk.Checkbutton(column_frame, text='Unique', variable=unique_var)
        unique_key_check.pack(side='left')

        self.attributes.append((name_entry, data_type_entry, pk_var, unique_var))

    def create_table(self):
        db_name = self.db_name_entry.get()
        table_name = self.table_name_entry.get()
        schema = {}
        primary_keys = []
        unique_keys = []

        for name_entry, type_entry, pk_var, unique_var in self.attributes:
            name = name_entry.get()
            data_type = type_entry.get()
            schema[name] = data_type
            if pk_var.get():
                primary_keys.append(name)
            if unique_var.get():
                unique_keys.append(name)

        response = requests.post(f"{SERVER_URL}/create_table", json={
            'database': db_name,
            'table_name': table_name,
            'schema': schema,
            'primary_key': primary_keys,
            'unique_keys': unique_keys
        })
        messagebox.showinfo("Response", response.json()['message'])

class Application(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Database Management System Client")
        self.geometry("500x500")

        tk.Button(self, text="Create Database", command=self.create_database).pack()
        tk.Button(self, text="Drop Database", command=self.drop_database).pack()
        tk.Button(self, text="Drop Table", command=self.drop_table).pack()
        tk.Button(self, text="Show Databases", command=self.show_databases).pack()

        self.db_tree = ttk.Treeview(self, columns=("Database Name",), show="headings")
        self.db_tree.heading("Database Name", text="Database Name")
        self.db_tree.pack(fill='both', expand=True)
        self.db_tree.bind("<Double-1>", self.on_db_click)

        self.table_tree = ttk.Treeview(self, columns=("Column Name", "Value"), show="headings")
        self.table_tree.heading("Column Name", text="Column Name")
        self.table_tree.heading("Value", text="Value")
        self.table_tree.pack(fill='both', expand=True)

    def create_database(self):
        name = simpledialog.askstring("Input", "Database Name:", parent=self)
        if name:
            response = requests.post(f"{SERVER_URL}/create_database", json={'name': name})
            messagebox.showinfo("Response", response.json()['message'])

    def drop_database(self):
        name = simpledialog.askstring("Input", "Database Name:", parent=self)
        if name:
            response = requests.post(f"{SERVER_URL}/drop_database", json={'name': name})
            messagebox.showinfo("Response", response.json()['message'])

    def drop_table(self):
        db_name = simpledialog.askstring("Input", "Database Name:", parent=self)
        table_name = simpledialog.askstring("Input", "Table Name:", parent=self)
        if db_name and table_name:
            response = requests.post(f"{SERVER_URL}/drop_table", json={'database': db_name, 'table_name': table_name})
            messagebox.showinfo("Response", response.json()['message'])

    def show_databases(self):
        self.db_tree.delete(*self.db_tree.get_children())
        databases = self.fetch_databases()
        for db in databases:
            self.db_tree.insert("", "end", values=(db['name'],))

    def fetch_databases(self):
        response = requests.get(f"{SERVER_URL}/list_databases")
        if response.ok:
            return response.json()
        else:
            messagebox.showerror("Error", "Failed to fetch databases")
            return []

    def on_db_click(self, event):
        item = self.db_tree.selection()[0]
        db_name = self.db_tree.item(item, "values")[0]
        self.show_tables(db_name)

    def show_tables(self, db_name):
        self.table_tree.delete(*self.table_tree.get_children())
        tables = self.fetch_tables(db_name)
        for table in tables:
            parent = self.table_tree.insert("", "end", text=table['name'], values=(table['name'], "Table"))
            for row in table['rows']:
                self.table_tree.insert(parent, "end", values=(row['column_name'], row['value']))

    def fetch_tables(self, db_name):
        response = requests.get(f"{SERVER_URL}/get_tables", params={'database': db_name})
        if response.ok:
            return response.json()
        else:
            messagebox.showerror("Error", f"Failed to fetch tables for {db_name}")
            return []

if __name__ == "__main__":
    app = Application()
    app.mainloop()

import unittest
from pymongo import MongoClient
from pymongo.errors import DuplicateKeyError
from index_manager import IndexManager

class TestIndexManager(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        """Set up database and index manager for testing."""
        cls.db_name = 'test_db'
        cls.collection_name = 'test_table'
        cls.field_name = 'email'
        cls.index_manager = IndexManager(uri="mongodb://localhost:27017/", db_name=cls.db_name)
        cls.client = MongoClient("mongodb://localhost:27017/")
        cls.db = cls.client[cls.db_name]
        cls.collection = cls.db[cls.collection_name]
        # Ensure the collection is empty before tests
        cls.collection.delete_many({})
        # Insert initial data
        cls.initial_docs = [
            {"_id": "doc1", "name": "Alice", "email": "alice@example.com"},
            {"_id": "doc2", "name": "Bob", "email": "bob@example.com"}
        ]
        cls.collection.insert_many(cls.initial_docs)

    def test_create_unique_index_and_insert_duplicate(self):
        """Test creating a unique index and trying to insert a duplicate entry."""
        result = self.index_manager.create_unique_index(self.collection_name, self.field_name)
        self.assertIn("created", result)
        with self.assertRaises(DuplicateKeyError):
            self.index_manager.insert_document(self.collection_name, {"name": "Alice", "email": "alice@example.com"})

    def test_delete_document_with_constraints(self):
        """Test deleting a document considering foreign key constraints."""
        # Assuming foreign key checks are integrated within the delete method
        response = self.index_manager.delete_document(self.collection_name, {"_id": "doc1"})
        self.assertIn("deleted", response)

    def test_update_and_validate_indexes(self):
        """Test updating a document and validating index integrity."""
        new_email = "new_alice@example.com"
        self.index_manager.update_document(self.collection_name, {"_id": "doc1"}, {"$set": {"email": new_email}})
        query_result = self.index_manager.query_index(self.collection_name, self.field_name, new_email)
        self.assertEqual(len(query_result), 1)
        self.assertEqual(query_result[0]['email'], new_email)

    def test_cleanup_indexes(self):
        """Test index cleanup for outdated or invalid entries."""
        # Test specific cleanup logic based on certain conditions
        self.index_manager.cleanup_indexes(self.collection_name)
        # Check if outdated entries are removed
        query_result = self.index_manager.query_index(self.collection_name, "outdated_field", "outdated_value")
        self.assertEqual(len(query_result), 0)

    @classmethod
    def tearDownClass(cls):
        """Clean up test database after all tests."""
        cls.db.drop_collection(cls.collection_name)
        cls.client.close()

if __name__ == '__main__':
    unittest.main()

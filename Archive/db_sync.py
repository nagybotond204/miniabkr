import json
import base64 
from bson import Binary
from pymongo import MongoClient
from datetime import datetime
from bson.decimal128 import Decimal128
from bson.objectid import ObjectId

class JSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Binary):
            return base64.b64encode(obj).decode('utf-8')  # Encode binary data as base64
        elif isinstance(obj, bytes):
            return base64.b64encode(obj).decode('utf-8')  # Encode bytes as base64
        elif isinstance(obj, datetime):
            return obj.isoformat()  # Convert datetime to an ISO format string
        elif isinstance(obj, Decimal128):
            return float(obj.to_decimal())  # Convert Decimal128 to float
        elif isinstance(obj, ObjectId):
            return str(obj)  # Convert ObjectId to string
        return json.JSONEncoder.default(self, obj)
    
class DBSync:
    def __init__(self, uri="mongodb://localhost:27017/", json_path="local_db.json"):
        self.client = MongoClient(uri)
        self.json_path = json_path
    
    #a function which pulls down databases from mongodb local server
    def pull_data_from_mongodb(self):
        databases = self.client.list_database_names()
        for database_name in databases:
            self.pull_data_from_mongodb_database(database_name)
    

    def construct_table_schema(self, collection_name, documents):
        schema = {}
        for document in documents:
            for key, value in document.items():
                if key not in schema:
                    schema[key] = type(value).__name__
        return schema
    
    def pull_data_from_mongodb_database(self, database_name):
        db = self.client[database_name]
        collections = db.list_collection_names()
        for collection_name in collections:
            self.pull_data_from_mongodb_collection(database_name, collection_name)
            
    def pull_data_from_mongodb_collection(self, database_name, collection_name):
        db = self.client[database_name]
        collection = db[collection_name]
        documents = list(collection.find())
        self.update_local_db(database_name, collection_name, documents)

    def update_local_db(self, database_name, collection_name, documents):
        data = self.read_from_json()
        if database_name not in data:
            data[database_name] = {}
        if collection_name not in data[database_name]:
            data[database_name][collection_name] = []

        # Convert Binary objects to bytes
        for document in documents:
            for key, value in document.items():
                if isinstance(value, Binary):
                    document[key] = bytes(value)

        data[database_name][collection_name] = documents
        self.write_to_json(data)
        
            
    def read_from_json(self):
        try:
            with open(self.json_path, 'r') as f:
                return json.load(f)
        except FileNotFoundError:
            # If the file doesn't exist, return an empty dictionary
            return {}
        except json.JSONDecodeError:
            # If the file is empty or not properly formatted JSON, return an empty dictionary
            return {}
    def load_local_data(self):
        with open(self.json_path, 'r') as file:
            return json.load(file)

    def push_data_to_mongodb(self):
        data = self.load_local_data()
        for database_name, content in data.items():
            db = self.client[database_name]
            for collection_name, documents in content.items():
                collection = db[collection_name]
                for document in documents:
                    if '_id' in document:
                        # Update if _id exists
                        collection.update_one({'_id': document['_id']}, {'$set': document}, upsert=True)
                    else:
                        # Insert new document if no _id is provided
                        collection.insert_one(document)
                print(f"Data pushed to {collection_name} in {database_name} database.")

    def write_to_json(self, data):
        with open(self.json_path, 'w') as f:
            json.dump(data, f, cls=JSONEncoder, indent=4)
    
